# Utworzenie poda z pliku
oc apply -f myapp-pod.yaml
# Pobiera liste podów
oc get pods
# Obserwowanie podów
oc get pods --watch
# Uruchamia VI do edycji definicji poda
oc edit pod/myapp-pod
# Zmiana edytora = nano
KUBE_EDITOR=nano oc edit pod/myapp-pod
# Usunięvie poda
oc delete pod/myapp-pod
